# Environment Setup

Before you begin, you'll need an IDE to start creating mobile apps with AnacleMobile. The following guide will detail the steps required to setup Visual Studio on your machine.

:::note

If you have previously completed the Anacle Framework training, and you have installed the **Mobile development with .NET workload**, you may proceed to the next [guide](02-solution-structure.md).

:::

## Visual Studio Setup

### Download Visual Studio

Begin by downloading the Visual Studio Installer from the following link: <https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=16>. 

Once downloaded, run the installer.

### Workloads

In the workloads tab, ensure the following workloads are selected:

* ASP.NET and web development
* .NET desktop development
* Mobile development with .NET
* .NET cross-platform development

![Workloads](2021-06-24-16-39-52.png)
![Workloads](2021-06-24-16-40-16.png)

Then, click on **Install**.

### Launch Visual Studio

Once the installation has completed, you may start Visual Studio and proceed to the next guide.
