# Tooling Setup

Before we begin, we will setup the **webapp** and **WebApi** project in Internet Information Services (IIS) on your machine.

## Setup webapp

For this training session, we will use a customized webapp that comes with the training repo that you've cloned earlier.

Create a new IIS application pointing to the webapp folder in the cloned training repo folder. This is the same as when you setup the webapp during your Anacle Framework training session.

Build the webapp and open in your browser. You should see the login page for Simplicity web. Download the application schema and execute the SQL query in SQL Management Studio (SSMS).

## Setup WebApi

Create another IIS application pointing to the **AnacleMobile.WebApi** folder. The full path to the folder is **AnacleMobile/AnacleMobile.WebApi**. 

Change the database connection string in the `web.config` file to match the database connection string which you have previously configured during your Anacle Framework training session.

Replace the `ErrorLogPath` and `ReportTempFolder` to any temp folder on your machine.

```xml title="web.config"
<add key="ErrorLogPath" value="REPLACE_THIS" />
<add key="ReportTempFolder" value="REPLACE_THIS" />
<add key="database" value="REPLACE_THIS" />
<add key="database_readonly" value="REPLACE_THIS" />
<add key="database_audit" value="REPLACE_THIS" />
```

Build the WebApi and open in your browser. You should the Swagger API listing page.

![image-20210708083800598](02-tooling-setup.assets/image-20210708083800598.png)

## Testing WebApi

From the Swagger API page, expand the **Test** API. You should see a list of operations/endpoints that can be called.

![image-20210707231206087](02-tooling-setup.assets/image-20210707231206087.png)

Expand the **/api/Tests** endpoint.

![image-20210707231224920](02-tooling-setup.assets/image-20210707231224920.png)

Click on **Try it out!**. You should see the response body and code.

![image-20210707231500180](02-tooling-setup.assets/image-20210707231500180.png)

This indicates that the API has been setup correctly.

## Using Insomnia

Instead of using the Swagger API page to test our APIs, we will be using *Insomnia* to test our APIs.

First, download the installer from the link [here](https://updates.insomnia.rest/downloads/windows/latest?app=com.insomnia.app&source=website). Once downloaded, double click on the installer and let the installer finish running.

The Insomnia window will open automatically when it finishes installation. Skip the analytics sharing dialog, API import dialog, and proceed to the **Dashboard** view.

![image-20210707231906658](02-tooling-setup.assets/image-20210707231906658.png)

In the Dashboard view, click on the **Insomnia** collection.

![image-20210707232058387](02-tooling-setup.assets/image-20210707232058387.png)

From the left pane, click on **No Environment**.

![image-20210708072401417](02-tooling-setup.assets/image-20210708072401417.png)

Click on **Manage Environments**.

![image-20210708072421436](02-tooling-setup.assets/image-20210708072421436.png)

From the left pane, click on **plus** icon next to **Sub Environments**, and select **Environment** from the dropdown menu.

![image-20210708072542952](02-tooling-setup.assets/image-20210708072542952.png)

Enter **localhost** as the name for the new environment.

![image-20210708072613181](02-tooling-setup.assets/image-20210708072613181.png)

From the right pane, in JSON format, enter the following property:

```json
"base_url": "http://localhost/<PATH_TO_YOUR_WEBAPI_IN_IIS>"
```

Replace `<PATH_TO_YOUR_WEBAPI_IN_IIS>` with the actual URL to your WebApi that was configured in IIS earlier. Close the **Manage Environments** window.

![image-20210708072754555](02-tooling-setup.assets/image-20210708072754555.png)

From the left pane, click on **No Environment** again, then select **Use localhost** from the dropdown menu. You should see the **No Environment** text changed to **localhost**.

![image-20210708072853906](02-tooling-setup.assets/image-20210708072853906.png)

Click on the **plus** icon as in above screenshot. Then select **New Request** from the dropdown menu.

![image-20210708072924988](02-tooling-setup.assets/image-20210708072924988.png)

Enter **Test API** as the name of the request, then create the request.

![image-20210708072945036](02-tooling-setup.assets/image-20210708072945036.png)

The **Test API** request now appears on the left pane of Insomnia. On the center pane, at the address bar, type in **base_url**, and wait for the autocomplete dropdown to appear, then press **Enter** on your keyboard.

![image-20210708073124839](02-tooling-setup.assets/image-20210708073124839.png)

Complete the URL by typing **/api/Tests**.

![image-20210708073146494](02-tooling-setup.assets/image-20210708073146494.png)

Finally, click on **Send**. You should see the response from the WebApi. The status code is **200 OK** and the response content is **Hello World**.

We can now use Insomnia to test our APIs in the next guide.

