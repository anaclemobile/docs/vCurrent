# Exercise

Before ending your training session, we have an exercise for you. 

In the Anacle Framework training session, you managed to implement the *Car* and *Car Rental* module on Simplicity web (`webapp`). Now, you will create the same module on AnacleMobile.

## Requirements Specification

### Backend API endpoints

1. Create a new API controller `CarRentalController.cs` in the *AnacleMobile.WebApi/Controllers* folder.

   1. Implement the following endpoints:

      | Name               | HTTP Method | Route           | Description                                                  |
      | ------------------ | ----------- | --------------- | ------------------------------------------------------------ |
      | `GetCarRentalList` | GET         | /api/CarRentals | This endpoint will fetch existing `OCarRental` from Simplicity. This allows us to view existing or past car rentals made so far. |
      | `SaveCarRental`    | POST        | /api/CarRentals | This endpoint will create/update `OCarRental` from our app. This allows us to make a new car rental or update an existing car rental details.<br />This endpoint should accept a single parameter of type `OMCarRental` from our mobile app. |

2. Create the necessary mapping object for the above API endpoints in the *AnacleMobile.WebApi/Models* folder:

   1. `OCarRental` -> `OMCarRental`

### Mobile app

1. Create the necessary mapping object in *AnacleMobile/Models*:

   1. `OMCarRental`

2. Implement the API services for our API endpoints:

   1. In the `IApiManager` interface, add the following methods:

      1. `Task<HttpResponseMessage> GetCarRentalList()`
      2. `Task<HttpResponseMessage> SaveCarRental(OMCarRental carRental)`

   2. In the `IWebApi` interface, add the following methods:

      1. ```csharp
         [Get("/api/CarRentals")]
         Task<HttpResponseMessage> GetCarRentalList(CancellationToken cancellationToken, [Header("Authorization")] string bearerToken);
         ```

      2. ```csharp
         [Post("/api/CarRentals")]
         Task<HttpResponseMessage> SaveCarRental([Body] OMCarRental carRental, CancellationToken cancellationToken, [Header("Authorization")] string bearerToken);
         ```

   3. In the `ApiManager` class, implement the following methods:

      1. `Task<HttpResponseMessage> GetCarRentalList()`
      2. `Task<HttpResponseMessage> SaveCarRental(OMCarRental carRental)`

   4. In the `OnlineService` class, implement the following methods:

      1. `Task<List<OMCarRental>> GetCarRentalList()`
      2. `Task<bool> SaveCarRental(OMCarRental carRental)`

3. Create a new page `CarRentalDetailPage.xaml` in the *AnacleMobile/Views* folder *(note: you can copy from `TodoDetailPage.xaml`)*.

   1. The page constructor should accept a parameter of type `OMCarRental`.
   2. The page should display the car rental details.
   3. The page should have a *submit* action to allows us to update an existing car rental record, or make a new car rental request.
   4. The submit action should call the API service `SaveCarRental` to push the `OMCarRental` object to our backend API.
   5. The page should close when submit action is successful.
   6. The page should have validation logic in place:
      1. `ToDate` cannot be earlier than `FromDate`
      2. `RentalPrice` cannot be less than 0.00
      3. `CarName` cannot be empty
      4. Either `RentByName` or `RentByID` cannot be empty

4. Create a new tab view `CarRentalInboxView.xaml` in the *AnacleMobile/Views* folder.

   1. The class `CarRentalInboxView` shall inherit from `BaseView` class *(note: you can copy from `TodoInboxView.xaml`)*.
   2. The tab view should have a `CollectionView` to display a *list of existing car rentals* from Simplicity.
   3. Override the `OnAppearing` method to call the API `GetCarRentalList` to retrieve the list of existing car rentals to be displayed.
   4. When you press on a car rental entry in the `CollectionView`, open the `CarRentalDetailPage` and display the car rental details on the page.
   5. There should be a floating action button to make a new car rental request (via `CarRentalDetailPage`),
   6. The user should be able view and update an existing car rental record, or make a new car rental request.
   7. The tab view should auto refresh when the `CarRentalDetailPage` is closed.

### Webapp

In Simplicity web, go to the *Car Rental* module search page to see the new car rentals submitted from your mobile app.

### Bonus

1. In the `CarRentalInboxView.xaml`, implement inbox pagination:
   1. Save the list of `OMCarRental` from the backend API into the app's local database.
   2. Paginate and load the list of `OMCarRental`s from the local database.
   3. Bind the loaded list to the `CollectionView`.
2. Create a login page and implement basic authentication for `CarRentalController` in *AnacleMobile.WebApi*.