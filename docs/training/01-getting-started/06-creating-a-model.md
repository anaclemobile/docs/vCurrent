# Creating a Model

A model class is a simple POCO class with a list of get/set properties. It usually has the following form:

```csharp
public class MyProfile {
    public string Name { get; set; }
    public int Age { get; set; }
}
```

A [POCO](https://en.wikipedia.org/wiki/Plain_old_CLR_object) class is *simple* object with no complex logic in it. They are mainly used for mapping `PersistentObject`s from Simplicity web. The mapping is required because we usually don't require **all** the fields, properties and the complex logic from a `PersistentObject`. As such, we've created a mapped `PersistentObject` class with the name `OMPersistentObject`. The `M` denotes that the class is used for mapping objects from the webapp.

## Create OMTodoItem

Let's create a model class for our `TodoDetailPage`. To create a model class:

1. Right click on the folder *AnacleMobile/Models* in the Solution Explorer
2. Click *Add*
3. Click *Class...*
4. Specify the name as `OMTodoItem.cs` and click the *Add* button

Replace the contents of `OMTodoItem.cs` with the following code:

```csharp title="AnacleMobile/Models/OMTodoItem.cs"
using System;

namespace AnacleMobile.Models
{
    public class OMTodoItem : OMPersistentObject
    {
        public string Description { get; set; }
        public DateTime? DueDateTime { get; set; }
        public int? IsCompleted { get; set; }
        public DateTime? CompletionDateTime { get; set; }
    }
}
```

The above code adds four properties to our `OMTodoItem` class. It also specifies that `OMTodoItem` should inherit from `OMPersistentObject`. The below shows the definition of the `OMPersistentObject` class:

```csharp title="AnacleMobile/Models/OMPersistentObject.cs"
public abstract class OMPersistentObject
{
    [PrimaryKey]
    public Guid ObjectID { get; set; }

    public string ObjectName { get; set; }
    public string ObjectNumber { get; set; }
    public int VersionNumber { get; set; } = 1;
    public DateTime? CreatedDateTime { get; set; }
    public string CreatedUser { get; set; }
    public DateTime? ModifiedDateTime { get; set; }
    public string ModifiedUser { get; set; }
    public int? IsDeleted { get; set; } = 0;

    public bool IsNew { get; set; } = true;
}
```

It basically mirrors the fields from `Anacle.DataFramework.PersistentObject` in Simplicity web.

## Data binding

We'll use our newly created model class in `TodoDetailPage`. 

Add a property `TodoItem` to `TodoDetailPage`:

```csharp title="AnacleMobile/Views/TodoDetailPage.xaml.cs"
public OMTodoItem TodoItem { get; }
```

Change the constructor to take in a single parameter of type `OMTodoItem`:

```csharp title="AnacleMobile/Views/TodoDetailPage.xaml.cs"
// Add a OMTodoItem parameter
//
public TodoDetailPage(OMTodoItem todoItem)
{
    InitializeComponent();
    
    // Set the property to the todoItem argument
    //
    TodoItem = todoItem; 
    
    // Call this method to notify our UI to refresh, so that we can see the values reflected on the UI
    //
    base.OnPropertyChanged(nameof(TodoItem)); 
}
```

In our XAML markup, bind the text field and date fields to our model object:

```xml title="AnacleMobile/Views/TodoDetailPage.xaml"
			...
			<material:MaterialTextField 
                Placeholder="Description"
                Text="{Binding TodoItem.Description}"/> <!-- Bind to OMTodoItem.Description -->
            <material:MaterialDateField
                Placeholder="Due Date Time"
                Date="{Binding TodoItem.DueDateTime}"/> <!-- Bind to OMTodoItem.DueDateTime -->
            <material:MaterialDateField
                Placeholder="Completion Date Time"
                Date="{Binding TodoItem.CompletionDateTime}"/> <!-- Bind to OMTodoItem.CompletionDateTime -->
			...
```

Finally, we need to create a new model and pass in to our `TodoDetailPage` in `App.xaml.cs`:

```csharp title="AnacleMobile/App.xaml.cs"
protected override async void OnStart()
{
    ...
    // Create a new model object
    //
    var model = new OMTodoItem
    {
        Description = "Hello World",
        DueDateTime = DateTime.Now
    };
    
    // Pass in to our TodoDetailPage
    //
    MainPage = new TodoDetailPage(model);
    ...
}
```

Rebuild and debug the app.

![image-20210707184344411](06-creating-a-model.assets/image-20210707184344411.png)

Add a breakpoint in `TodoDetailPage.AppBarButton4Command`:

![image-20210707184511658](06-creating-a-model.assets/image-20210707184511658.png)

Go to the app, change the *Hello World* text to *Hello Anacle*, then, click the Send button. The breakpoint will be hit and your app will be paused. Let's inspect the value of  `TodoItem.Description`. In the debug watch window, switch to the *Watch 1* window. Double click on *Add item to watch*, and enter `TodoItem.Description`. You should see the value reflected as *Hello Anacle*.

![image-20210707184753006](06-creating-a-model.assets/image-20210707184753006.png)

Our `TodoItem`'s description was changed without any explicit code assignment. This is possible as we've bound our `TodoItem.Description` property with the *Description* text field on our UI. Any changes from the UI will update our model object instantly.

Remove the breakpoint, and proceed to the next guide.



