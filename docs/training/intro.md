---
sidebar_position: 1
slug: /training/
---

# Overview

Welcome to the training guide for AnacleMobile. Over the course of this training guide, you'll learn what are the different components available in AnacleMobile and how they fit in as a whole.

AnacleMobile is an opinionated set of reusable components built on top of Xamarin.Forms. Experience programming with Xamarin.Forms will be good to have but not required for this training guide.

## Prerequisites

* Completed Anacle Framework training.
* Comfortable programming in C#.
* Have a GitLab account associated with your Anacle personal email account

## Take away

* You'll have a general understanding of programming mobile apps with AnacleMobile.
* You'll be able to create new web APIs for your mobile app.
* You'll be able to create new modules for your mobile app.

## What will not be covered

* Deep dive into C# programming.
* Framework variations between different projects: in the real world, different projects will have different customizations on top of AnacleMobile, which arises due to different requirements and/or specific use cases. This training guide will not cover these variations.
