# Creating a Page - Part 1

In the previous guide, we managed to launch our app with the Android Emulator. We will now look closely at the UI components available to create a page.

![page-anatomy](04-creating-a-page-1.assets/page-anatomy.png)

The components that make up the `HomePage` are as follows:

| Component       | Description                                                  |
| --------------- | ------------------------------------------------------------ |
| `AppBar`        | Component that implements the top app bar. It supports up to four action buttons and a page title. |
| `TabView`       | Component that implements a tab view which contains a list of `TabViewItem`s to be displayed. |
| `TabViewItem`   | Component that implements a tab view item. The tab icon and text can be set here. |
| `TodoInboxView` | Inherits from `BaseView`. <br />The content of our page will reside here. |
| `BaseView`      | Abstract base class for views.<br />It includes the `AppBar`, the main content, and an activity indicator. <br />When we create a new view, we should inherit from this class.<br />When we create a new page, we should use this class as the root element of the page. |

## Anatomy of a Page

Using the example above, let us look at the source codes of `HomePage` and `HelloWorldPage`:

```xml title="AnacleMobile/Views/HomePage.xaml"
<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
                xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
                xmlns:ios="clr-namespace:Xamarin.Forms.PlatformConfiguration.iOSSpecific;assembly=Xamarin.Forms.Core"
                xmlns:ui="clr-namespace:MobileUIControls.Views;assembly=MobileUIControls"
                xmlns:views="clr-namespace:AnacleMobile.Views;assembly=AnacleMobile"
                x:Class="AnacleMobile.Views.HomePage"
                NavigationPage.HasNavigationBar="False"
                ios:Page.UseSafeArea="true">
    <!--THE FIRST ELEMENT OF THIS PAGE IS A TABVIEW-->
    <ui:TabView x:Name="MainTab">
        ...
```

```xml title="AnacleMobile/Views/HelloWorldPage.xaml"
<?xml version="1.0" encoding="utf-8" ?>
<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             x:Class="AnacleMobile.Views.HelloWorldPage">
    <ContentPage.Content>
        <StackLayout>
            <!--MAKE THE CHANGE HERE-->
            <!--Change "Hello World" to "Hello Anacle"-->
            <Label Text="Hello World"
                   VerticalOptions="CenterAndExpand" 
                   HorizontalOptions="CenterAndExpand" />
        </StackLayout>
    </ContentPage.Content>
</ContentPage>
```

The XAML markup starts with the `ContentPage` class, which indicates that this is a `Page`. This is the same as HelloWorldPage. 

In HelloWorldPage the first element is a `StackLayout` and a `Label` with the text *Hello World*. Whereas in the HomePage, the first element is a `TabView`, and there are three `TabViewItem`s under the TabView. Each TabViewItem corresponds to a tab on the HomePage.

The *Todos* `TabViewItem` XAML markup:

```xml title="AnacleMobile/Views/HomePage.xaml"
		...		

		<ui:TabViewItem
            Text="Todos"
            TextColor="Gray"
            SelectedTextColor="{DynamicResource BaseLightBlue}"
            ContentViewType="{x:Type views:TodoInboxView}"> <!--WE SET THE VIEW TO BE DISPLAYED HERE-->
            <ui:TabViewItem.Icon>
                <FontImageSource
                    Glyph="&#xe241;"
                    Size="Large"
                    FontFamily="MaterialIconsRegular"
                    Color="{DynamicResource BaseLightDark}"/>
            </ui:TabViewItem.Icon>
            <ui:TabViewItem.SelectedIcon>
                <FontImageSource
                    Glyph="&#xe241;"
                    Size="Large"
                    FontFamily="MaterialIconsRegular"
                    Color="{DynamicResource BaseLightBlue}"/>
            </ui:TabViewItem.SelectedIcon>
        </ui:TabViewItem>

		...
```

`TabViewItem` properties:

| Property            | Description                                                  |
| ------------------- | ------------------------------------------------------------ |
| `Text`              | Gets or sets the tab item text.<br />In our case, the text is *Todos.* |
| `TextColor`         | The tab item text color when it is not selected.             |
| `SelectedTextColor` | The tab item text color when it is selected.                 |
| `Icon`              | The tab item icon when it is not selected.                   |
| `SelectedIcon`      | The tab item icon when it is selected.                       |
| `ContentViewType`   | The type of the View to display.<br />In our case, we set it to `TodoInboxView`. The tab view will then show the contents of `TodoInboxView` when selected. |

:::info

The `FontImageSource.Glyph` property is set using the font codepoint from Material icons.

To see the list of Material icons available, click [here](https://fonts.google.com/icons). For more info about `FontImageSource`, click [here](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/text/fonts#display-font-icons).

:::

Finally, we look at the markup for `TodoInboxView`:

```xml title="AnacleMobile/Views/TodoInboxView.xaml"
<?xml version="1.0" encoding="utf-8" ?>
<ui:BaseView xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             xmlns:ui="clr-namespace:MobileUIControls.Views;assembly=MobileUIControls"
             xmlns:models="clr-namespace:AnacleMobile.Models;assembly=AnacleMobile"
             xmlns:xct="http://xamarin.com/schemas/2020/toolkit"
             xmlns:material="clr-namespace:XF.Material.Forms.UI;assembly=XF.Material"
             x:Class="AnacleMobile.Views.TodoInboxView"
             x:Name="CurrentView"
             Title="Todo List">
    <StackLayout HorizontalOptions="CenterAndExpand"
                 VerticalOptions="CenterAndExpand">
        <!--MAKE THE CHANGE HERE-->
        <!--Change "Hello World" to "Hello Anacle"-->
        <Label Text="Hello World"/>
    </StackLayout>
</ui:BaseView>
```

You'll notice that the contents of the TodoInboxView is the same as the HelloWorldPage. The difference is that the TodoInboxView is *nested* in a TabView, which is then *nested* in the HomePage.

