# Local database

Suppose we wanted to continue using our mobile app without an Internet connection, we would not be able to do so. The app will try to retrieve the todo items from our backed API and fail. We would not be able to see any items on our app. 

In this guide, we will implement a `Manager` class for our `OMTodoItem` model class to enable offline local database storage of our model objects. Once we have retrieved the todo items from our backend API, we will store the items in our local database. In the absence of Internet connection, we will retrieve the todo items from our local database instead of our backend API.

:::note

The current version of AnacleMobile uses [sqlite-net](https://github.com/praeclarum/sqlite-net) as the local database library. As the library is not actively maintained anymore, we will be dropping sqlite-net and migrating to [Entity Framework Core](https://github.com/dotnet/efcore) for the next release of AnacleMobile.

:::

## `Database` class

The `Database` class contains the definition of the list of *tables* for our local database. Before we begin creating our `Manager` class, we will have to add our model class as a table inside the `Database` class.

In the Solution Explorer, expand the *Models* folder and find the *Database.cs* file. Then, make the following changes:

```csharp title="Database.cs"
void Initialize()
{
    ...
}

// Add the below two lines of code, after the "Initialize()" method
//
[MasterData]
public List<OMTodoItem> TodoItems { get; set; }

```

The `MasterData` attribute denotes that the `TodoItems` property is a table in our local database.

## `Manager` class

We are now ready to create our `Manager` class. We start by creating *TodoManager.cs*:

1. In the Solution Explorer, right click on the *Managers* folder
2. Click on *Add*
3. Click on *Class...*
4. Enter the name *TodoManager.cs*
5. Click *Add*

Open the newly created file *TodoManager.cs*, and replace the contents of the file with the following code:

```csharp title="TodoManager.cs"
using System.Collections.Generic;
using System.Linq;
using AnacleMobile.Models;

namespace AnacleMobile.Managers
{
	// The Manager class serves as an abstraction for all the data access logic that we
    // will be using in our views/pages. This is so that it allows us to easily switch to another 
    // database provider (e.g. EF.Core) by changing this class instead of changing all the views/pages.
    //
    public class TodoManager
    {
        // This method will take a list of todo items and save into our local DB.
        //
        public void UpdateTodoItems(List<OMTodoItem> items)
        {
            // We use the standard method provided by our `Database` class.
            // This method will insert or replace a list of objects into our local DB.
            //
            App.Database.AddItems(items);
        }

        // Get a list of todo items.
        //
        public List<OMTodoItem> GetTodoItems()
        {
            // We use a SQL query to fetch the list of todo items from our local DB.
            //
            var sql = string.Join(" ",
                "SELECT *",
                "FROM OMTodoItem",
                "ORDER BY CreatedDateTime DESC");
            var list = App.Database.Query(typeof(OMTodoItem), sql);
            
            // As the resultant list is of type "object", we have to 
            // explicitly cast them to "OMTodoItem".
            //
            return list.Select(i => (OMTodoItem) i).ToList();
        }
    }
}
```

With our `Manager` class ready, we will have to start using it in `TodoInboxView`.

Open the file *TodoInboxView.xaml.cs*, add a new readonly field as follows: 

```csharp title="TodoInboxView.xaml.cs"
public partial class TodoInboxView
{	
	...
	
    // Add a new field to use our newly created TodoManager
    //
	private readonly TodoManager _todoManager = new TodoManager();
	
	...
}
```

Then, replace the code in `RefreshCommand` with the following code:

```csharp title="TodoInboxView.xaml.cs"
public ICommand RefreshCommand => new Command(async () =>
{
    try
    {
        if (IsBusy) return;
        IsBusy = true;

        // We check for Internet connectivity using the helper method 
        // provided by Xamarin.Essentials.
        //
        if (Connectivity.NetworkAccess != NetworkAccess.None)
        {
            // Retrieve the todo items from our backend API
            //
            var itemsFromServer = await OnlineService.Instance.GetTodoList();
            
            // Save the list of items to our local database
            //
            _todoManager.UpdateTodoItems(itemsFromServer);
        }

        // We then fetch the todo items from our local database
        //
        var items = _todoManager.GetTodoItems();
        
        // Bind the items to our CollectionView
        //
        TodoList.Clear();
        TodoList.AddRange(items);
    }
    finally
    {
        IsBusy = false;
    }
});
```

Build and debug the app. Try to turn off wifi/data on your Android Emulator after the todo list is displayed initially. The app should now display the todo items from our local database instead of fetching from our backend API.

:::info

For more info on Xamarin.Essentials, click [here](https://docs.microsoft.com/en-us/xamarin/essentials/).

:::

## Pagination

Pagination is a common feature of any mobile app. Ideally, the paging should occur on the local DB instead of at the backend API DB. This is so that the backend API DB will not be overloaded with paging requests every now and then. As paging is a DB intensive operation, even more so when the data volume is huge, the general recommendation is to offload the work to the local DB.

In this case, we simply push all the items to the mobile app. The mobile app will then save the items to its local DB and perform paging from the local DB. We also get the added benefit of having offline access to our todo list in the absence of Internet connectivity.

Let's add pagination to our `TodoInboxView`. Replace the code for `GetTodoItems` in *TodoManager.cs* with the following code:

```csharp title="TodoManager.cs"
// Add two parameters, 'offset' and 'limit'.
//
public List<OMTodoItem> GetTodoItems(long offset = 0, long limit = 100)
{
    var sql = string.Join(" ",
        "SELECT *",
        "FROM OMTodoItem",
        "ORDER BY CreatedDateTime DESC",
        $"LIMIT {limit}",					// Add LIMIT clause
        $"OFFSET {offset}");				// Add OFFSET clause
    var list = App.Database.Query(typeof(OMTodoItem), sql);
    return list.Select(i => (OMTodoItem) i).ToList();
}
```

The LIMIT clause specifies that we only want to retrieve the first 100 rows for our query. The OFFSET clause specifies *how many* rows we want to skip, before retrieving the rows for our query.

:::info

For more info on SQLite LIMIT and OFFSET clause, click [here](https://www.sqlite.org/lang_select.html#the_limit_clause).

:::

Next, in *TodoInboxView.xaml*, we add the attributes `RemainingItemsThreshold` and `RemainingItemsThresholdReachedCommand` to our CollectionView as follows:

```xml title="TodoInboxView.xaml"
...
<RefreshView IsRefreshing="{Binding IsBusy, Source={x:Reference CurrentView}}"
             Command="{Binding RefreshCommand, Source={x:Reference CurrentView}}">
    <!-- ADD `RemainingItemsThreshold` and `RemainingItemsThresholdReachedCommand` to our `CollectionView` -->
    <CollectionView ItemsSource="{Binding TodoList, Source={x:Reference CurrentView}}"
                    RemainingItemsThreshold="5"
                    RemainingItemsThresholdReachedCommand="{Binding LoadMoreCommand, Source={x:Reference CurrentView}}">
        <CollectionView.ItemTemplate>
            <DataTemplate>
            ...
```

Then, add a new field `_currentLoadedIndex` in *TodoInboxView.xaml.cs*:

```csharp title="TodoInboxView.xaml.cs"
private long _currentLoadedIndex;
```

In the same class, we add the `LoadMoreCommand` property using the following block of code:

```csharp title="TodoInboxView.xaml.cs"
public ICommand LoadMoreCommand => new Command(async () =>
{
    try
    {
        if (IsBusy) return;
        IsBusy = true;
        
        // If we have less than 100 items, there's no need to load more items
        //
        if (TodoList.Count < 100) return;
        
        // Increment our counter, which will be used as the offset for 
        // retrieving todo items using TodoManager
        //
        _currentLoadedIndex += 100;

        // Get the list of todo items with offset
        //
        var items = _todoManager.GetTodoItems(_currentLoadedIndex);
        
        // Add the list of todo items to our CollectionView
        //
        TodoList.AddRange(items);
    }
    finally
    {
        IsBusy = false;
    }
});
```

In the `RefreshCommand` property, add the following lines of code before the line `var items = _todoManager.GetTodoItems();`:

```csharp title="TodoInboxView.xaml.cs"
// Reset the currently loaded index
// If we don't do this, our `LoadMoreCommand` will 
// not be synchronized after we refresh our CollectionView
//
_currentLoadedIndex = 0;

// Add the above lines of code
//
var items = _todoManager.GetTodoItems();
```

The `RefreshCommand` will reset the `_currentLoadedIndex` counter, clear the `TodoList` and fetch the first 100 items from the local DB. Whereas the `LoadMoreCommand` will simply fetch the next 100 items from our local DB and add the items to `TodoList`.

Build and debug the app.
