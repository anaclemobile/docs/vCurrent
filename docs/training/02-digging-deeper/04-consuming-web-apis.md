# Consuming Web APIs

With our backend WebApi prepared, we can now consume the newly added APIs from our mobile app.

## Add the Todo APIs

In the *AnacleMobile* project, look for the folder *Interfaces*, and open the file *IWebApi.cs*.

Add the following lines of code:

```csharp title="IWebApi.cs"
public interface IWebApi
{ 
   ...

   // Add the following lines of code in between the IWebAPI interface
   //
   [Get("/api/Todos")]
   Task<HttpResponseMessage> GetTodoList(CancellationToken cancellationToken, [Header("Authorization")] string bearerToken);

   [Post("/api/Todos")]
   Task<HttpResponseMessage> SaveTodoItem([Body] OMTodoItem item, CancellationToken cancellationToken, [Header("Authorization")] string bearerToken);
   
   ...
}
```

In the same folder, find and open the file *IApiManager.cs*.

Add the following lines of code:

```csharp title="IApiManager.cs"
public interface IApiManager
{
   ...

   // Add the following lines of code
   //
   Task<HttpResponseMessage> GetTodoList();
   Task<HttpResponseMessage> SaveTodoItem(OMTodoItem item);

   ...
}
```

In the *AnacleMobile* project, look for the folder *Services*, and open the file *ApiManager.cs*.

Add the following lines of code to the end of the file, before the last closing bracket of the class definition block:

```csharp title="ApiManager.cs"
public class ApiManager : IApiManager
{
   ...

   // Add the following lines of code
   //
   public async Task<HttpResponseMessage> GetTodoList()
   {
      var cts = new CancellationTokenSource();
      var task = RemoteRequestAsync(webApi.GetApi(Priority.UserInitiated).GetTodoList(cts.Token, BearerEncrypted));
      runningTasks.Add(task.Id, cts);
      return await task.ConfigureAwait(false);
   }

   public async Task<HttpResponseMessage> SaveTodoItem(OMTodoItem item)
   {
      var cts = new CancellationTokenSource();
      var task = RemoteRequestAsync(webApi.GetApi(Priority.UserInitiated).SaveTodoItem(item, cts.Token, BearerEncrypted));
      runningTasks.Add(task.Id, cts);
      return await task.ConfigureAwait(false);
   }

   ...
}
```

In the same folder, find and open the file *OnlineService.cs*.

Add the following lines of code:

```csharp title="OnlineService.cs"
public class OnlineService
{
   ...

   // Add the following lines of code
   //
   public async Task<List<OMTodoItem>> GetTodoList()
   {
      var response = await ApiManager.GetTodoList();
      return await response.DeserializeObjectAsync<List<OMTodoItem>>();
   }

   public async Task<bool> SaveTodoItem(OMTodoItem item)
   {
      var response = await ApiManager.SaveTodoItem(item);
      return response.IsSuccessStatusCode;
   }

   ...
}
```

To be able to call our API endpoints from our mobile app, the following 4 files needs to be changed -- for each API endpoint:

1. IWebApi.cs
2. IApiManager.cs
3. ApiManager.cs
4. OnlineService.cs

This is a required step as we are using a combination of libraries that make up our HTTP client stack in AnacleMobile:

| Library          | Description                                                  |
| ---------------- | ------------------------------------------------------------ |
| Refit            | Auto generates an API client from an interface.<br />Can be injected with any HttpClient. In our case, we used ModernHttpClient. |
| Fussilade        | Adds HTTP request prioritization logic.                      |
| ModernHttpClient | Provides a better API surface than the standard HttpClient from .NET. |
| Polly            | Provides auto retry capabilities for transient faults (network connection lost or timeouts). |

:::note

In the upcoming AnacleMobile vNext release, we will be changing this behavior and migrate to use an API client that will be auto generated at *compile time* (instead of at runtime with Refit).

We will be dropping Refit, Fussilade and ModernHttpClient from our HTTP client stack.

:::

## Using the Todo APIs

We now have the ability to consume our web APIs in our mobile app. Now, we need to actually use it in `TodoDetailPage` and `TodoInboxView`.

In `TodoDetailPage.xaml.cs`, replace the `AppBarButton4Command` property with the following code:

```csharp title="TodoDetailPage.xaml.cs"
public ICommand AppBarButton4Command => new Command(async () =>
{
   try
   {
       if (IsBusy) return;
       IsBusy = true;

       var (isValid, message) = IsValid();
       if (!isValid)
       {
           await DisplayAlert("", message, "OK");
           return;
       }

       // Call our SaveTodoItem API here and pass in our TodoItem
       //
       var isSuccessful = await OnlineService.Instance.SaveTodoItem(TodoItem);
       if (isSuccessful)
       {
           await DisplayAlert("", "Successfully save the todo item", "OK");
           await Navigation.PopModalAsync();
           ActionCallback?.Invoke(TodoItem);
       }
       else
       {
           // Display an error message if it fails
           //
           await DisplayAlert("", "Failed to save the todo item. Please try again.", "OK");
       }
   }
   catch (Exception ex)
   {
       await ExceptionHelper.LogExceptionAsync(ex);
   }
   finally
   {
       IsBusy = false;
   }
});
```

Then, in `TodoInboxView.xaml.cs`, add the following block of code:

```csharp title="TodoInboxView.xaml.cs"
// Override the OnAppearing method
// The OnAppearing method will be called everytime when the TodoInboxView is *visible*
// 1. When you close the TodoDetailPage, the TodoInboxView will be visible, OnAppearing will be called
// 2. When you switch tabs in the HomePage, if the Todo tab is selected, TodoInboxView will be visible,
//    OnAppearing will be called
//
public override void OnAppearing()
{
    base.OnAppearing();
    
    // Execute the RefreshCommand
    // We will change our RefreshCommand to get a list of TodoItems from our API
    // 
    RefreshCommand.Execute(null);
}
```

Finally, in the same file, add the following lines of code in the `RefreshCommand` property, right after the `IsBusy = true;` line:

```csharp title="TodoInboxView.xaml.cs"
...
IsBusy = true;

// Add the following lines
// Get the list of TodoItems from our API, and add it to our TodoList collection.
//
var items = await OnlineService.Instance.GetTodoList();
TodoList.ReplaceRange(items);
...
```

After the above changes, our `TodoDetailPage` and `TodoInboxView` is now fully API driven.

:::info

To learn more about the async-await keyword, click [here](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/async/).

:::

## Base URL configuration

Before we run our app, we need to change the WebApi base URL config in the `Config` class.

Open a Command Prompt or PowerShell window, and type `ipconfig`. Find your machine's IP address, note it down.

In the Solution Explorer, find the folder *Config*, and open the *Config.cs* file. Replace the value of `ApiUrl`, with the full path to your WebApi configured in IIS, using your machine's IP address.

```csharp title="Config.cs"
// Replace the value for this line of code.
// The below is an example. Replace "192.168.0.116" with your machine's IP address.
//
public static string ApiUrl = "http://192.168.0.116/mobileapi";
```

## Run the app

Build and run the app. Try to add new todo items and edit existing todo items. They should now be saved to your backend database. Close and run the app again, you should see the previously saved todo items being populated in the CollectionView.

