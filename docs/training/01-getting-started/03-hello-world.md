# Hello World

To run our app, we can either use a physical Android/iOS device, or an emulator/simulator. Do note that iOS apps will require a macOS machine to be built and executed. As such, this guide will mainly focus on using the Android Emulator to run our app.

## Android Emulator

Try running the app in debug mode by pressing on the *play* button or use the Visual Studio hotkey *F5*. You should see a prompt to install the Android SDK and Android Emulator.

:::note

If you have launched the Android Emulator from Visual Studio previously, the below prompt may not appear. Simply proceed to the next step and try to run the app in debug mode.

:::

Click *Accept*.

![image-20210707161514425](03-hello-world.assets/image-20210707161514425.png)

Then, click *Create*.

![image-20210707161539960](03-hello-world.assets/image-20210707161539960.png)

Click *Accept* again.

![image-20210707161555179](03-hello-world.assets/image-20210707161555179.png)

Wait for the installation to complete.

![image-20210707161618584](03-hello-world.assets/image-20210707161618584.png)

## Launch the app

Once the AVD has been installed, try to debug the app again by pressing on the *play* button or *F5*.

As we are running our app for the first time, it will take some time for the app to be built and installed on our emulator. The app will automatically launch in our emulator once it has finished building and installation.

Once the app is launched, there should be a page with the text *Hello World* like the screenshot below.

![image-20210707155621133](03-hello-world.assets/image-20210707155621133.png)

Leave the app running in debug mode and move on to the next step.

:::info

*Fast deployment* and *incremental build* are features provided by the Xamarin SDK that greatly reduces the amount of time required to build, rebuild and debug your app during the app development cycle. Make sure it is turned on during app development.

:::

## XAML Hot Reload

In *AnacleMobile/Views/HelloWorldPage.xaml*, try changing the *Hello World* text to something else, for example, *Hello Anacle*. Then, switch back to your app in the emulator. You should see your change reflected almost immediately in your app.

```xml title="AnacleMobile/Views/HelloWorldPage.xaml"
<?xml version="1.0" encoding="utf-8" ?>
<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             x:Class="AnacleMobile.Views.HelloWorldPage">
    <ContentPage.Content>
        <StackLayout>
            <!--MAKE THE CHANGE HERE-->
            <!--Change "Hello World" to "Hello Anacle"-->
            <Label Text="Hello World"
                   VerticalOptions="CenterAndExpand" 
                   HorizontalOptions="CenterAndExpand" />
        </StackLayout>
    </ContentPage.Content>
</ContentPage>
```

XAML hot reload is a useful feature that enables hot reloading your XAML code changes on the fly with your app running in debug mode.

We will explain more about the code above in the next guide.

:::info

Currently, hot reload will only work for XAML file changes. C# code changes will **not** be hot reloaded and will require a rebuild of the app.

For more info about XAML hot reload, click [here](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/xaml/hot-reload).

:::

## Application class

Let's change our startup page to use the `HomePage` class. Currently, the startup page is set to `HelloWorldPage`, which simply displays the text *Hello World* (or *Hello Anacle* after the above change).

To make the change, open the file *AnacleMobile/App.xaml.cs*, and find the `OnStart` method. You'll notice there's a lot going on in this class, but we'll ignore them for now. 

At line 56, change the `MainPage = new HelloWorldPage();` line to `MainPage = new HomePage();`. After this change, stop your app. Then, build and run your app again. As we have made a C# code change, an app rebuild is required.

```csharp title="AnacleMobile/App.xaml.cs"
protected override async void OnStart()
{
    try
    {
        DbConstants.DatabasePath = Path.Combine(DbConstants.DatabaseFolderPath, "user");
        Directory.CreateDirectory(DbConstants.DatabasePath);
        DbConstants.DatabasePath = Path.Combine(DbConstants.DatabasePath, DbConstants.DatabaseFilename);

        // MAKE THE CHANGE HERE
        // Change "HelloWorldPage" to "HomePage"
        //
        MainPage = new HelloWorldPage();

        ExceptionHelper.StartBackgroundService();
    }
    catch (Exception ex)
    {
        await ExceptionHelper.LogExceptionAsync(ex);
    }
}
```

:::info

The `App` class is where most of the app initialization logic will be, i.e. database initialization, startup page and starting background services.

:::

You should now see that your startup page has the *Hello World* text. However, we now have an app bar at the top with the title *Todo List*, and a tab bar at the bottom with three tabs (*Todos*, *Rentals* and *Settings*).

![image-20210707155621133](03-hello-world.assets/image-20210707133914827.png)

Again, we will change the text *Hello World* to *Hello Anacle*. Open the file *AnacleMobile/Views/TodoInboxView.xaml*, and make the change as below:

```xml title="AnacleMobile/Views/TodoInboxView.xaml"
<?xml version="1.0" encoding="utf-8" ?>
<ui:BaseView xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             xmlns:ui="clr-namespace:MobileUIControls.Views;assembly=MobileUIControls"
             xmlns:models="clr-namespace:AnacleMobile.Models;assembly=AnacleMobile"
             xmlns:xct="http://xamarin.com/schemas/2020/toolkit"
             xmlns:material="clr-namespace:XF.Material.Forms.UI;assembly=XF.Material"
             x:Class="AnacleMobile.Views.TodoInboxView"
             x:Name="CurrentView"
             Title="Todo List">
    <StackLayout HorizontalOptions="CenterAndExpand"
                 VerticalOptions="CenterAndExpand">
        <!--MAKE THE CHANGE HERE-->
        <!--Change "Hello World" to "Hello Anacle"-->
        <Label Text="Hello World"/>
    </StackLayout>
</ui:BaseView>
```

You should see the text changed to *Hello Anacle*.

