# Next Steps

1. Inform your trainer that you have completed the exercise and have your exercise reviewed.
2. Come back to this site to check out other topics such as push notifications, publishing to play store/app store, and [more](/topics/).

