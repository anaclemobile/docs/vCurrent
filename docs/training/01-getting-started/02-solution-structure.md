# Solution Structure

In this guide, we will take a look at the projects available in the AnacleMobile training solution.

## Clone training source code

To begin, we will clone the training source code repo from GitLab.

```bash
git clone https://gitlab.com/anaclemobile/training/xamarin.forms-vcurrent-starter
```

Open the solution `AnacleMobile.sln` with Visual Studio. The cloned repo includes a copy of the Anacle Framework training source code which you have acquired from your previous training session.

## Solution structure explained

The Solution Explorer should have the below structure. We will explain each project on how they fit in the solution as a whole.

```text
- AnacleMobile
  - Platforms
    - AnacleMobile.Android
    - AnacleMobile.iOS
  - WebApi
    - AnacleMobile.WebAPI
    - LogicLayer
  - AnacleMobile
  - MobileUIControls
```

![A screenshot of the solution structure.](assets/20210706_224858_image.png)

### AnacleMobile

The `AnacleMobile` project will be where most of your app logic and UI code reside. You should be making changes almost 90% of your time in this project.

Important folders:


| Directory  | Remarks                     |
| ------------ | :---------------------------- |
| Assets     | Image assets and font files |
| Config     | Configuration classes       |
| Enums      | Enum constants              |
| Extensions | Extension methods       |
| Interfaces | Interfaces that will be implmented by platform-specific projects |
| Managers   | Local database managers |
| Models     | Model/[POCO](https://en.wikipedia.org/wiki/Plain_old_CLR_object) classes |
| Services   | API services |
| Views      | Pages/views |

### MobileUIControls

This project contains the common UI components to be used in your `AnacleMobile` project. Components such as `AppBar`, `TabView` and `TabViewItem` will reside here. Usually, there is no need to make any changes in this project unless you have certain customizations to be made to the common UI components.

### Platforms/AnacleMobile.Android

The platform-specific project for Android. It serves as the entry point for your Android app. Usually, there is no need to make any changes in this project unless you have to. The general rule of thumb is to avoid making changes in platform-specific projects unless necessary.

### Platforms/AnacleMobile.iOS

This is the platform-specific project for iOS. It serves as the entry point for your iOS app. Usually, there is no need to make any changes in this project unless you have to.

### WebApi/AnacleMobile.WebAPI

This project contains all the backend API endpoints for our `AnacleMobile` app. Any API backend changes will be made in this project.

Important folders:


| Folder      | Remarks |
| ------------- | --------- |
| App_Start   | ASP.NET Web API configurations |
| Attributes  | Authorization attributes, filter attributes and data mapping attributes |
| Controllers | API controllers and endpoints |
| Extensions  | Extension methods |
| Models      | Model/POCO classes (this is almost the same as in `AnacleMobile`) |
| Resources   | Localization resource files |
| Utilities   | Internal helper and utility classes |

### WebApi/LogicLayer

`LogicLayer` from Anacle Framework. This is shared between `AnacleMobile.WebAPI` and `webapp` (and other projects from the main solution `abell.sln`). 

