/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'AnacleMobile',
  tagline: 'AnacleMobile Documentation',
  url: 'https://anaclemobile.gitlab.io/',
  baseUrl: '/docs/vCurrent/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/anacle.png',
  organizationName: 'anaclesys', // Usually your GitHub org/user name.
  projectName: 'docs/vCurrent', // Usually your repo name.
  themeConfig: {
    sidebarCollapsible: false,
    navbar: {
      title: 'AnacleMobile',
      logo: {
        alt: 'AnacleMobile',
        src: 'img/anacle.png',
      },
      items: [
        {
          type: 'doc',
          docId: 'training/intro',
          position: 'left',
          label: 'Training',
        },
        {
          to: 'topics',
          position: 'left',
          label: 'Topics',
        },
        {
          position: 'left',
          label: 'Support',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Training',
          items: [
            {
              label: 'Getting Started',
              to: 'training',
            },
            {
              label: 'Digging Deeper',
              to: 'training/digging-deeper/recap',
            },
            {
              label: 'Exercise',
              to: 'training/exercise',
            },
          ],
        },
        {
          title: "Topics",
          items: [
            {
              label: 'Anti-patterns',
              to: 'topics/anti-patterns'
            }
          ]
        },
        {
          title: 'Support',
          items: [
            {
              label: 'Code Snippets',
              to: 'under-construction',
            },
            {
              label: 'Knowledge Base',
              to: 'under-construction',
            },
          ],
        },
      ],
      copyright: `Copyright © 2021 Anacle Systems Limited. Prepared by Zhen Zhi.`,
    },
    prism: {
      additionalLanguages: ['csharp'],
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // editUrl:
          //   'https://github.com/facebook/docusaurus/edit/master/website/',
          routeBasePath: '/',
          // sidebarItemsGenerator: async function ({
          //   defaultSidebarItemsGenerator,
          //   ...args
          // }) {
          //   /** @type {any[]} */
          //   let sidebarItems = await defaultSidebarItemsGenerator(args);

          //   // Exclude under-construction docs
          //   //
          //   sidebarItems = sidebarItems.filter(i => !i.id || (i.id && !i.id.includes('features/under-construction')));

          //   return sidebarItems;
          // },
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
