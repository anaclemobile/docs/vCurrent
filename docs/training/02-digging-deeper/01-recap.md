# Recap

In the previous section, we've managed to build and debug an AnacleMobile app with Visual Studio and Android Emulator.

In this section, we will work on making our app API driven by consuming web APIs. We will first begin by setting up our web API project with IIS, which you have installed from your web training session earlier. Then, we will install a [tool](https://insomnia.rest/download) to test our web API before we use it in our mobile app. Finally, we will create a few API controllers and endpoints that will be consumed by our mobile app.

At the end of this section, our mobile app will be fully API driven.

