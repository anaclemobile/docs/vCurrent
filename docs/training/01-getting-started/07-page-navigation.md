# Page Navigation

Our app now shows the `TodoDetailPage` when its launched. That's all good but it's time to introduce some interactivity. In this guide, we will implement page navigation, so that we can navigate from our `TodoInboxView` to our `TodoDetailPage`.

In our `TodoInboxView`, we will implement a scrolling list view to display a list of todo items. When a todo item is clicked, the app will navigate to our `TodoDetailPage`, and display the todo item details. We will also add a floating action button to the inbox view, so that when it is clicked, it will navigate to `TodoDetailPage` with a new todo item to be filled up.

## Starter code

In `App.xaml.cs`, replace the `OnStart` method with the below code:

```csharp title="AnacleMobile/App.xaml.cs"
protected override async void OnStart()
{
    try
    {
        DbConstants.DatabasePath = Path.Combine(DbConstants.DatabaseFolderPath, "user");
        Directory.CreateDirectory(DbConstants.DatabasePath);
        DbConstants.DatabasePath = Path.Combine(DbConstants.DatabasePath, DbConstants.DatabaseFilename);

        MainPage = new HomePage();

        ExceptionHelper.StartBackgroundService();
    }
    catch (Exception ex)
    {
        await ExceptionHelper.LogExceptionAsync(ex);
    }
}
```

Rebuild and debug your app, you should see the *Hello World* text and three tabs.

Stop debugging. Replace the code in `TodoInboxView.xaml` with the below code:

```xml title="AnacleMobile/Views/TodoInboxView.xaml"
<?xml version="1.0" encoding="utf-8" ?>
<ui:BaseView xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             xmlns:ui="clr-namespace:MobileUIControls.Views;assembly=MobileUIControls"
             xmlns:models="clr-namespace:AnacleMobile.Models;assembly=AnacleMobile"
             xmlns:xct="http://xamarin.com/schemas/2020/toolkit"
             xmlns:material="clr-namespace:XF.Material.Forms.UI;assembly=XF.Material"
             x:Class="AnacleMobile.Views.TodoInboxView"
             x:Name="CurrentView"
             Title="Todo List">
    <AbsoluteLayout>
        <!-- The markup for the scrolling list of todo items -->
        <StackLayout AbsoluteLayout.LayoutBounds="0,0,1,1" AbsoluteLayout.LayoutFlags="All">
            <!-- The RefreshView must be the parent of the CollectionView, to enable the `pull to refresh` functionality -->
            <RefreshView IsRefreshing="{Binding IsBusy, Source={x:Reference CurrentView}}"
                         Command="{Binding RefreshCommand, Source={x:Reference CurrentView}}">
        		<!-- We add a CollectionView so that we can view a list of OMTodoItems -->
                <CollectionView ItemsSource="{Binding TodoList, Source={x:Reference CurrentView}}">
                    <CollectionView.ItemTemplate>
                        <DataTemplate>
                            <!-- The TouchEffect binds the SelectCommand, so that we can open the detail page when clicked on -->
                            <StackLayout
                                xct:TouchEffect.NativeAnimation="True"
                                xct:TouchEffect.Command="{Binding SelectCommand, Source={x:Reference CurrentView}}"
                                xct:TouchEffect.CommandParameter="{Binding .}"
                                Spacing="0">
                                <StackLayout 
                                    Padding="10, 20, 10, 20"
                                    x:DataType="models:OMTodoItem" >
                                    <!-- Show the todo item description -->
                                    <Label
                                        Text="{Binding Description}"
                                        FontSize="Medium"/>
                                </StackLayout>
                                <BoxView HeightRequest="1" Color="Gray" Opacity="0.1"/>
                            </StackLayout>
                        </DataTemplate>
                    </CollectionView.ItemTemplate>
                    <CollectionView.EmptyView>
                        <!-- The UI to display when the TodoList is empty -->
                        <ContentView>
                            <StackLayout HorizontalOptions="CenterAndExpand" VerticalOptions="CenterAndExpand">
                                <Image>
                                    <Image.Source>
                                        <FontImageSource
                                            Glyph="&#xe4f7;"
                                            FontFamily="MaterialIconsRegular"
                                            Size="128"
                                            Color="Gray"/>
                                    </Image.Source>
                                </Image>
                                <Label 
                                    Text="You have no upcoming tasks."/>
                            </StackLayout>
                        </ContentView>
                    </CollectionView.EmptyView>
                </CollectionView>
            </RefreshView>
        </StackLayout>

        <!-- The markup for the floating action button -->
        <!-- Note the binding of the NewTodoCommand here -->
        <StackLayout AbsoluteLayout.LayoutBounds="0,0,1,1" 
                     AbsoluteLayout.LayoutFlags="All"
                     VerticalOptions="End">
            <material:MaterialIconButton
                CornerRadius="50"
                WidthRequest="68"
                HeightRequest="68"
                HorizontalOptions="End"
                VerticalOptions="End"
                Margin="0,0,10,10"
                BackgroundColor="{DynamicResource BaseLightBlue}"
                Command="{Binding NewTodoCommand, Source={x:Reference CurrentView}}">
                <material:MaterialIconButton.Image>
                    <FontImageSource Glyph="&#xe145;"
                                     FontFamily="MaterialIconsOutlinedRegular"
                                     Size="Large"
                                     Color="White" />
                </material:MaterialIconButton.Image>
            </material:MaterialIconButton>
        </StackLayout>
    </AbsoluteLayout>
</ui:BaseView>
```

Replace the code in `TodoInboxView.xaml.cs` with the below code:

```csharp title="AnacleMobile/Views/TodoInboxView.xaml.cs"
using System.Windows.Input;
using AnacleMobile.Models;
using Xamarin.CommunityToolkit.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AnacleMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TodoInboxView
    {
        public TodoInboxView()
        {
            InitializeComponent();
            CurrentAppBar.Button1IsBackButton = false;
        }

        // This will be bound to the CollectionView so that we can view a scrolling list of OMTodoItems
        // When we add new items or remove items from this list, the CollectionView will reflect the change via binding
        //
        public ObservableRangeCollection<OMTodoItem> TodoList { get; } = new ObservableRangeCollection<OMTodoItem>();

        // The action to be executed when we click on a todo item 
        //
        public ICommand SelectCommand => new Command(async obj =>
        {
            try
            {
                if (IsBusy) return;
                
                // Setting `IsBusy = true` will make the RefreshView show the refreshing icon
                //
                IsBusy = true;

                // DO STUFFS HERE
                // Open the detail page with an existing todo item
                //
            }
            finally
            {
                IsBusy = false;
            }
        });

        // The action to be executed when we click on the floating action button
        //
        public ICommand NewTodoCommand => new Command(async () =>
        {
            try
            {
                if (IsBusy) return;
                IsBusy = true;

                // DO STUFFS HERE
                // Open the detail page with a new todo item
                //
            }
            finally
            {
                IsBusy = false;
            }
        });

        // The action to be executed when we pull to refresh
        //
        public ICommand RefreshCommand => new Command(async () =>
        {
            try
            {
                if (IsBusy) return;
                IsBusy = true;

				// DO STUFFS HERE
                // You can refresh the TodoList here
            }
            finally
            {
                IsBusy = false;
            }
        });
    }
}
```

Take a few minutes to review the comments for the codes you just copied.

Launch the app in debug mode. You should see the following UI:

![image-20210707212043844](07-page-navigation.assets/image-20210707212043844.png)

Play around with the app:

1. Try clicking on the floating action button
2. Try pulling the todo list downwards (see if it refreshes)
3. Try to switch to another tab

## Implement logic to create new todo item

In `TodoDetailPage.xaml.cs`, add a new property as follows:

```csharp title="TodoDetailPage.xaml.cs"
public Action<OMTodoItem> ActionCallback { get; set; }
```

Replace the constructor with the following code:

```csharp title="TodoDetailPage.xaml.cs"
public TodoDetailPage(OMTodoItem item, Action<OMTodoItem> action = null)
{
    InitializeComponent();
    TodoItem = item;
    base.OnPropertyChanged(nameof(TodoItem));
    
    // Set the ActionCallback property here
    //
    ActionCallback = action;
}
```

Replace the `AppBarButton4Command` method with the following code:

```csharp title="TodoInboxView.xaml.cs"
public ICommand AppBarButton4Command => new Command(async () => 
{
    await DisplayAlert("", "Successfully save the todo item", "OK");
    await Navigation.PopModalAsync();
    
    // Call the callback action, pass in our todo item
    //
    ActionCallback?.Invoke(TodoItem);
});
```

In `TodoInboxView.xaml.cs`, add the following lines of code after the `IsBusy = true;` statement in the `NewTodoCommand` property:

```csharp title="TodoInboxView.xaml.cs"
IsBusy = true;

// Insert the following lines of code
//
var newTodo = new OMTodoItem();
await Navigation.PushModalAsync(new NavigationPage(new TodoDetailPage(newTodo, (callbackItem) =>
{
    // We add the new todo item to our TodoList
    //
    TodoList.Add(callbackItem);
})));
```

Add the following lines of code after the `IsBusy = true;` statement in the `SelectCommand` property:

```csharp title="TodoInboxView.xaml.cs"
IsBusy = true;

// Insert the following lines of code
//
var item = (OMTodoItem)obj;
await Navigation.PushModalAsync(new NavigationPage(new TodoDetailPage(item, (callbackItem) =>
{
    // We remove the existing todo item, and add the one from the callback to make the CollectionView refresh
    //
    TodoList.Remove(callbackItem);
    TodoList.Add(callbackItem);
})));
```

Launch the app in debug mode.

Try clicking on the floating action button, enter a todo description and click on the send button. You should see the new todo item displayed in the inbox view. Then, try click on the same todo item and edit its description. You should see the inbox view reflect the changes when the detail page is closed.

## Add form field validation

If you try to create a new todo item and click on the send/save button without entering anything, the popup dialog will say it has *Successfully save the todo item*. After which you will have a list of empty rows in your `CollectionView`. This is because the todo items doesn't have a description.

Let's add some basic field validation. In `TodoDetailPage.xaml.cs`, add the following block of code:

```csharp title="TodoDetailPage.xaml.cs"
// The return type is a combination of a boolean and a string.
//
public (bool isValid, string message) IsValid()
{
    if (string.IsNullOrWhiteSpace(TodoItem.Description))
        return (false, "Description cannot be empty");

    return (true, null);
}
```

In `AppBarButton4Command`, add the following lines of code before the `await DisplayAlert(...)` line:

```csharp title="TodoDetailPage.xaml.cs"
// Add the following lines of code
//
var (isValid, message) = IsValid();
if (!isValid)
{
    await DisplayAlert("", message, "OK");
    return;
}

// Add the above lines of code before the DisplayAlert statement
//
await DisplayAlert("", "Successfully save the todo item", "OK");
```

Rebuild and debug your app. Now, try to create a new todo item without entering any details, you should not be able to do so.

