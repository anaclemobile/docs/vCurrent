# Creating a Page - Part 2

Let's create a new page named *TodoDetailPage.xaml*. To create a new page:

1. Right click on the folder *AnacleMobile/Views* in the Solution Explorer
2. Click on *Add*
3. Click on *New item...*
4. In the popup dialog box, select *Content Page*
5. Specify the name as *TodoDetailPage.xaml* (you can omit the .xaml file extension if you wish)

When the new page is created, you'll see that the contents of the page is the same as *HelloWorldPage*. Let's add *BaseView* to the page. Copy the below code, and replace **all** the code in *TodoDetailPage.xaml*. Then, save the file.

```xml title="AnacleMobile/Views/TodoDetailPage.xaml"
<?xml version="1.0" encoding="utf-8" ?>
<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             xmlns:material="clr-namespace:XF.Material.Forms.UI;assembly=XF.Material"
             xmlns:ui="clr-namespace:MobileUIControls.Views;assembly=MobileUIControls"
             x:Class="AnacleMobile.Views.TodoDetailPage"
             x:Name="CurrentView"
             NavigationPage.HasNavigationBar="False">
    <ui:BaseView Title="Todo">
        <ui:BaseView.AppBarButton4Source>
            <FontImageSource
                Glyph="&#xe163;"
                Size="Large"
                FontFamily="MaterialIconsRegular"
                Color="{DynamicResource BaseLightDark}"/>
        </ui:BaseView.AppBarButton4Source>
        <StackLayout Padding="10" Spacing="16">
            <material:MaterialTextField 
                Placeholder="Description"/>
            <material:MaterialDateField
                Placeholder="Due Date Time"/>
            <material:MaterialDateField
                Placeholder="Completion Date Time"/>
        </StackLayout>
    </ui:BaseView>
</ContentPage>
```

The above code adds BaseView to our new page. The base view contains three UI fields, *Description*, *Due Date Time* and *Completion Date Time*. It also sets the app bar button to have the *Send* icon from Material icons. The UI fields we used are from the library [XF.Material](https://github.com/Baseflow/XF-Material-Library).

Go back to our *App.xaml.cs* file, and change the `MainPage = new HomePage();` to `MainPage = new TodoDetailPage();`. Then, debug the app. You should see the below page.

![image-20210707173316260](05-creating-a-page-2.assets/image-20210707173316260.png)

You'll notice that the back button and send button has no effect. That's because we have not bound any command to the buttons. The back button won't work here because the TodoDetailPage is already at the top of the navigation stack. How do we know that? We changed the `MainPage` property of `App` to `new TodoDetailPage()`. We'll get back on the concept of page navigation later.

For the send button, we'll bind a command to it now. A command is simply an action that will be executed when the button is pressed. Let's bind a command to our app bar button:

```xml title="AnacleMobile/Views/TodoDetailPage.xaml"
<?xml version="1.0" encoding="utf-8" ?>
<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             xmlns:material="clr-namespace:XF.Material.Forms.UI;assembly=XF.Material"
             xmlns:ui="clr-namespace:MobileUIControls.Views;assembly=MobileUIControls"
             x:Class="AnacleMobile.Views.TodoDetailPage"
             x:Name="CurrentView"
             NavigationPage.HasNavigationBar="False">
    <!-- MAKE THE CHANGE TO BASEVIEW -->
    <ui:BaseView Title="Todo"
                 AppBarButton4Command="{Binding AppBarButton4Command, Source={x:Reference CurrentView}}"> 
        ...
        ...
    </ui:BaseView>
</ContentPage>
```

Then, in our page code behind, add the command property as follows:

```csharp title="AnacleMobile/Views/TodoDetailPage.xaml.cs"
[XamlCompilation(XamlCompilationOptions.Compile)]
public partial class TodoDetailPage : ContentPage
{
    public TodoDetailPage()
    {
        InitializeComponent();
    }

    // ADD THE BUTTON COMMAND HERE
    //
    public ICommand AppBarButton4Command => new Command(async () =>
    {
        await DisplayAlert("", "Hello World", "OK");
    });
    
    ...
    ...
}
```

As we've made a C# code change, stop debugging the app, then, build and debug the app again.

Once the app is launched, try clicking on the send button. You should see a popup dialog showing the text *Hello World*. We've successfully bound a command to our app bar button.

## BindingContext

Let's look closely at the XAML binding syntax we made:

```xml
<ui:BaseView Title="Todo"
             AppBarButton4Command="{Binding AppBarButton4Command, Source={x:Reference CurrentView}}">
```

The XAML binding syntax has the following forms:

```xml
<!-- Binds a property from the current BindingContext to SomeAttribute -->
<SomeElement SomeAttribute="{Binding <PATH>}"/>

<!-- Binds a property from the source to SomeAttribute -->
<SomeElement SomeAttribute="{Binding <PATH>, Source={<SOURCE>}}"/>
```

In our case, SomeElement is BaseView, and SomeAttribute is AppBarButton4Command.  We used the second syntax because we have not set a BindingContext for our page. If you remove the Source, the command won't be bound to the button as the XAML engine won't be able to find the command from our page.

The Source we specified references the current page, which is TodoDetailPage itself. The CurrentView keyword is set at the root element of our page using the x:Name attribute.

```xml title="AnacleMobile/Views/TodoDetailPage.xaml"
<ContentPage ...
             x:Name="CurrentView"
             ...>
```

An alternative way would be to set the BindingContext to our page. Let's try that by making the following changes:

```xml title="AnacleMobile/Views/TodoDetailPage.xaml"
<?xml version="1.0" encoding="utf-8" ?>
<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             xmlns:material="clr-namespace:XF.Material.Forms.UI;assembly=XF.Material"
             xmlns:ui="clr-namespace:MobileUIControls.Views;assembly=MobileUIControls"
             x:Class="AnacleMobile.Views.TodoDetailPage"
             x:Name="CurrentView"
             NavigationPage.HasNavigationBar="False"
             BindingContext="{Binding Source={x:Reference CurrentView}}"> <!-- ADD THIS LINE OF CODE -->
    <ui:BaseView Title="Todo"
                 AppBarButton4Command="{Binding AppBarButton4Command}"> <!-- REMOVE THE SOURCE HERE -->
        ...
        ...
    </ui:BaseView>
</ContentPage>
```

The button still works and the popup is displayed, as we expected. We'll discuss more about data binding when we start creating model classes in the next guide.

:::info

For more info on BindingContext, click [here](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/app-fundamentals/data-binding/basic-bindings).

:::