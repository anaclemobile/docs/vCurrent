# API Controllers

With our WebApi configured in IIS and Insomnia installed, we're ready to create API endpoints for our mobile app.

## Creating an API controller

In the Solution Explorer, expand the *WebApi* folder, and look for the *AnacleMobile.WebApi* project. 

Right click on the *Models* folder, and add a new C# class with the name `OMTodoItem`. Replace the contents of the file with the below code:

```csharp title="OMTodoItem.cs"
using System;

namespace AnacleMobile.WebAPI.Models.Todo
{
    public class OMTodoItem : OMPersistentObject
    {
        public string Description { get; set; }
        public DateTime? DueDateTime { get; set; }
        public int? IsCompleted { get; set; }
        public DateTime? CompletionDateTime { get; set; }
    }
}
```

We've just added a new model class to our WebApi. You'll notice that the class definition is exactly the same as with the model class `OMTodoItem` we created in our AnacleMobile project. Usually they mirror one another, but that's not always necessary.

Right click on the *Controllers* folder, and add a new C# class with the name `TodoController`. Replace the contents of the file with the below code:

```csharp title="TodoController.cs"
using System.Web.Http;
using Anacle.DataFramework;
using AnacleMobile.WebAPI.Extensions;
using AnacleMobile.WebAPI.Models.Todo;
using LogicLayer;

namespace AnacleMobile.WebAPI.Controllers
{
    // We set the route prefix here, so that all the API endpoints (or methods) in this controller,
    // will have the "api/Todos" prefix.
    // In Insomnia, we would specify "<BASE_URL>/api/Todos" as the API endpoint URL. <BASE_URL> here
    // is what we've configured in our Insomnia Environment settings.
    //
    [RoutePrefix("api/Todos")]
    public class TodoController : BaseApiController
    {
        // The HttpGet attribute specifies that this method/endpoint should only accept 
        // incoming HTTP requests with the GET method.
        // 
        // The Route attribute specifies the URL path of this method/endpoint.
        // Combined with the RoutePrefix attribute of the controller, the effective route
        // of this endpoint would be "api/Todos". If the Route is "Submit", the 
        // effective route would be "api/Todos/Submit".
        //
        [HttpGet, Route("")]
        public IHttpActionResult GetTodoItems()
        {
            // We use DataFramework to query a list of OTodoItems
            //
            var list = TablesLogic.tTodoItem.LoadList(Query.True);
            
            // We map OTodoItems to OMTodoItems
            //
            var mList = list.ToMappedObjectList<OMTodoItem>();
            
            // Return them to the API caller, in our case, it will be Insomnia or our mobile app
            //
            return Ok(mList);
        }

        // The HttpPost attribute specifies that this method/endpoint should only accept
        // incoming HTTP requests with the POST method.
        //
        // The FromBody attribute specifies that this endpoint should accept a request body content
        // that matches our mapped model class OMTodoItem.
        //
        [HttpPost, Route("")]
        public IHttpActionResult SaveTodoItem([FromBody] OMTodoItem mItem)
        {
            // We map the OMTodoItem to OTodoItem
            //
            var item = mItem.ToPersistentObject<OTodoItem>();
            
            // Save to our database and commit
            // This is using an extension method that wraps the following code:
            //
            // using (var c = new Connection()) {
            // 	item.Save();
            // 	c.Commit();
            // }
            //
            item.SaveAndCommit();
            
            // Return 200 OK to the caller without any body content
            //
            return Ok();
        }
    }
}
```

Review the comments in the copied code.

## Swagger/Open API

Build the WebApi project, and open the Swagger page in your browser. If the Swagger page is displayed, it indicates that our WebApi is good to go.

Swagger is a API specification format that allows easy consumption of APIs by API consumers, as well as having an added benefit of automatically generating a API documentation site like the below.

![image-20210708083717630](03-api-controllers.assets/image-20210708083717630.png)

You should see that the *Todo* API is added. Click on **List Operations** to see a list of operations/endpoints available.

![image-20210708084032975](03-api-controllers.assets/image-20210708084032975.png)

The first `GET /api/Todos` corresponds to `GetTodoItems()` method in `TodoController`.

The second `POST /api/Todos` corresponds to `SaveTodoItem(OMTodoItem mItem)` in `TodoController`.

## Test our API endpoints

Let's add two new requests in Insomnia.

### GetTodoList

Add a new request with the name `Get Todo List` and URL `/api/Todos` (don't forget to prefix with your `base_url` environment variable).

![image-20210708084432575](03-api-controllers.assets/image-20210708084432575.png)

Click **Send**. You should get a *200 OK* with an empty list. That's okay because we have not added any TodoItem to our database yet.

### SaveTodoItem

Add another request with the name `Save Todo Item` and URL `/api/Todos`. Change the request method to **POST**.

![image-20210708084639703](03-api-controllers.assets/image-20210708084639703.png)

Under the URL bar, click on the **Body** tab. Select **JSON** from the dropdown menu.

![image-20210708084739934](03-api-controllers.assets/image-20210708084739934.png)

Type in the following JSON code:

```json
{
    "Description": "HELLO WOLRD!!!"
}
```

![image-20210708084821156](03-api-controllers.assets/image-20210708084821156.png)

Click **Send**. You should get a *200 OK* response with *no body returned*.

![image-20210708084846652](03-api-controllers.assets/image-20210708084846652.png)

That's okay because in our `SaveTodoItem` method in the `TodoController`, we only specified to return `Ok()` without any content.

Switch to the `Get Todo List` request, and click **Send** again. You should now see the newly created TodoItem returned to us.

![image-20210708085124908](03-api-controllers.assets/image-20210708085124908.png)

## HTTP methods

In our `TodoController`, we added two endpoints. One `GET` endpoint and another `POST` endpoint. This follows the RESTful API pattern. RESTful API simply means that, as much as possible, we should design our API endpoints in combination of the HTTP method to be used.

| HTTP method | Description                  |
| ----------- | ---------------------------- |
| GET         | Get a resource               |
| POST        | Create a new resource        |
| PUT         | Create or replace a resource |
| DELETE      | Delete an existing resource  |
| PATCH       | Update an existing resource  |

In AnacleMobile, *there is no requirement for our APIs to be 100% RESTful*. This allows us the flexibility of designing our API to be as efficient as possible by:

1. Reducing the total number of API calls required to perform an operation

1. Reducing the number of roundtrip API calls

The only drawback is reduced readability as we've deviated from the RESTful API standard.

In a usual AnacleMobile project, you should see we use mainly the `GET` and `POST` method for our API endpoints.