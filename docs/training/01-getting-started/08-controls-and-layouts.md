# Controls and Layouts

In the previous guide, we added a bunch of XAML markups to our `TodoInboxView.xaml` file. We will now explain some of the controls and layouts we have used.

## Controls

Controls (or `View`s) are UI objects you see on a `Page`. In AnacleMobile, we use a combination of standard controls available in *Xamarin.Forms*, custom controls in *MobileUIControls*, and third-party controls in libraries such as *XF.Material* and *Syncfusion.Xamarin*. In an earlier guide, we added the following controls to `TodoDetailPage.xaml` and `TodoInboxView.xaml`.

| Control                                                      | Namespace        | Description                                                  |
| ------------------------------------------------------------ | ---------------- | ------------------------------------------------------------ |
| `material:MaterialTextField`                                 | XF.Material      | Used for text input.                                         |
| `material:MaterialDateField`                                 | XF.Material      | Used for datetime input.                                     |
| `material:MaterialIconButton`                                | XF.Material      | Used for touch input. In our case, we've set the border radius to make it resemble a [floating action button](https://material.io/components/buttons-floating-action-button). |
| [`Label`](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/text/label) | Xamarin.Forms    | Used to display text.                                        |
| [`Image`](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/images?tabs=windows) | Xamarin.Forms    | Used to display an image. The image can be a physical image file (.png or .jpg), or we can use a font icon like [Material icons](https://fonts.google.com/icons) which we've embedded in AnacleMobile. |
| [`BoxView`](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/boxview) | Xamarin.Forms    | Used to display a rectangular box. In our case, we used this control as a list separator with a `HeightRequest` of `1`. |
| [`CollectionView`](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/collectionview/) | Xamarin.Forms    | Used to display a collection of objects.                     |
| [`RefreshView`](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/refreshview) | Xamarin.Forms    | Used to provide `CollectionView` with a pull to refresh functionality. It can also be used in other controls or layouts. |
| `AppBar`                                                     | MobileUIControls | Convenience component to add action buttons and a title to our page. |

:::info

For a full list of standard controls available in Xamarin.Forms, click [here](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/xaml/xaml-controls).

In addition, AnacleMobile has its own set of custom controls available in the *MobileUIControls* project. The `AppBar` is an example of a custom control.

:::

## Layouts

Layouts are `View`s  that houses other controls or layouts beneath it. They form the backbone of a page. A page content must start with a layout. In an earlier guide, we added the following layouts to `TodoInboxView.xaml`.

| Layout                                                       | Namespace        | Description                                                  |
| ------------------------------------------------------------ | ---------------- | ------------------------------------------------------------ |
| [`StackLayout`](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/layouts/stacklayout) | Xamarin.Forms    | Used to display other controls or layouts in a one-dimensional stack. |
| [`AbsoluteLayout`](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/layouts/absolutelayout) | Xamarin.Forms    | Used to display other controls or layouts overlapping one another. |
| BaseView                                                     | MobileUIControls | Convenience layout component that includes an `AppBar` and an activity indicator. |

:::info

For a full list of standard layouts available in Xamarin.Forms, click [here](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/controls/layouts).

In addition, AnacleMobile has its own set of custom layouts available in the *MobileUIControls* project. The `BaseView` and `TabView` are examples of custom layouts.

:::
