# Inbox Pagination

Infinite scrolling list view is a recurring pattern in mobile app development. Almost every mobile app will have a list of items that can be scrolled infinitely, at least until there are no items left.

In this section, we will change our `TodoInboxView.xaml` to utilize our local database for pagination.

## Saving to local database

In the previous section, we fetched the list of `TodoItem`s from our web API and data bound it to our `CollectionView`. Now, we will change the behaviour. We will first save the `TodoItem`s into our local database. Subsequently, we will then change `TodoList` variable to use the list of `TodoItem`s from our local database via a local database query.

